package zadanie9;


import java.util.ArrayList;
import java.util.List;

public class User {
    private String name;
    private String username;
    private String password;
    private String surname;
    private static ArrayList<User> users= new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public static ArrayList<User> getUsers() {
        return users;
    }

    public static void setUsers(ArrayList<User> users) {
        User.users = users;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    public User(String name, String username, String password, String surname) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.surname = surname;
        User.users.add(this);
        //dodaje  obiekt do arrayListy
        System.out.println("doddaje  objekt"+this);
        System.out.println("wielskosc listy"+users.size());


    }
    public static List<User> getAll(){
        return users;
    }
}
